
# birdoro

Birdoro is a silly pomodoro timer made for the [Virtual Pet Jam](https://itch.io/jam/virtual-pet-jam).

Code made in [raylib](https://www.raylib.com/), pixel art made with aseprite, sounds made with VCV Rack.

Builds for windows and linux on the [itch.io page](https://nonmateria.itch.io/birdoro).

## Building

Having raylib installed, building it with `make` from the root of the folder will output an executable in the `birdoro/` folder

## Instructions 

- press 1 = change theme
- press 2 = make transparent
- press 3 = lower volume ( emits beep to check )
- press 4 = raise volume ( emits beep to check )
- press up-down arrows / mouse wheel  = change time 

settings are saved on exit and restored on start

press enter or space key (or click the time) to start timer 

bird doesn't like to be clicked 

## license

Nicola Pisanti MIT License 2022
