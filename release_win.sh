#!/usr/bin/fish

rm birdoro_win64.zip

make clean 
make CROSS=WINDOWS
rm birdoro/storage.data
zip -r birdoro_win64.zip birdoro

exit