
Birdoro is a pomodoro timer acting as a bird (or the other way around?)

controls:
	press 1 = change theme
	press 2 = make transparent
	press 3 = lower volume ( emits beep to check )
	press 4 = raise volume ( emits beep to check )
	press up-down arrows / mouse wheel  = change time 

settings are saved on exit and restored on start

press enter or space key to start timer 

bird doesn't like to be clicked 

made for Virtual Pet Jam 
https://itch.io/jam/virtual-pet-jam

by nonmateria 
http://nonmateria.com
